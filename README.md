# **Robotframework + Docker Workshop**

## Description
Welcome to the basic automation workshop with robotframework as an automation tool and docker for tests excution
environment

## **Topics**
- Robotframework (What is it and basic setup)
- Project setup (First test case execution)
- Docker (What is it and basic setup)
- Test execution with selenoid

### Robotframework
Robot Framework is a generic open source automation framework for acceptance testing, acceptance test driven
development (ATDD), and robotic process automation (RPA). It has easy-to-use tabular test data syntax and it
utilizes the keyword-driven testing approach. Its testing capabilities can be extended by test libraries
implemented either with Python or Java, and users can create new higher-level keywords from existing ones using
the same syntax that is used for creating test cases

### Docker
Package Software into Standardized Units for Development, Shipment and 
Deployment



Execute Robotframework project
> robot -d {results output} -i {test tags}  {robot suites location}

> pabot --processes {number of threads} -d {results output} -i {test tags}  {robot suites location}

Examples:
> robot -d results -i test  test_suites

> pabot --processes 2 -d results -i test test_suites