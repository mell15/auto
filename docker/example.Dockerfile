#Download images
FROM ubuntu:18.04

#update system
RUN apt update && apt install -y

#Setup python environment
RUN apt install python3.7 -y
RUN apt install python3-pip -y

RUN mkdir auto

#Copy requirements.txt
COPY requirements.txt auto/

#Installing libraries
RUN pip3 install -r auto/requirements.txt

ENTRYPOINT [ "tail","-f","/dev/null" ]