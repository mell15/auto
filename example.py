from selenium import webdriver
import time


def example():
    capabilities = {
        "browserName": "chrome",
        "version": "76",
        "enableVNC": True,
        "enableVideo": False
    }

    driver = webdriver.Remote(
        command_executor="http://selenoid-workshop:4444/wd/hub",
        desired_capabilities=capabilities)

    #driver = webdriver.Chrome()
    driver.get("http://34.205.174.166/")
    driver.maximize_window()
    time.sleep(5)
    assert driver.find_element_by_id("masthead").is_displayed()
    time.sleep(5)
    driver.quit()


example()