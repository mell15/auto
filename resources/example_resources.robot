*** Settings ***
Documentation    Suite description
Library  SeleniumLibrary
Library  StringFormat
Variables  ../locators/example_locators.yaml

*** Keywords ***
navigate to QA Playground
   # Open Browser    http://34.205.174.166/   Chrome
    Open Browser        http://34.205.174.166/    Chrome   ${None}   http://selenoid-workshop:4444/wd/hub
    Maximize Browser Window
    Set Selenium Speed    0.25

is QA Playground displayed
    Wait Until Element Is Visible   id:masthead   10


Search For Hoodie
    [Documentation]  this is a test
    [Arguments]  ${hoodie_name}
    Input Text  ${home_page.search_field}  ${hoodie_name}
    Press Keys  ${home_page.search_field}  ENTER

is your hoodie displayed?
    [Arguments]  ${hoodie_name}
    ${hoodie}=   format string  ${results_page.item_result}  ${hoodie_name}
    Wait Until Element Is Visible   ${hoodie}   10
