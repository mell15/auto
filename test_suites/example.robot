*** Settings ***
Documentation    Suite description
Library  SeleniumLibrary
Library  StringFormat
Resource  ../resources/example_resources.robot
Suite Setup  navigate to QA Playground
Suite Teardown  Close Browser

*** Variables ***
${HOODIE}   Hoodie with Zipper

*** Test Cases ***
First Test Case
    [Tags]   test
    is QA Playground displayed

Search For Hoodie Test
    [Tags]  test    regression  smoke   adhoc
    Search For Hoodie   hoodie
    is your hoodie displayed?   ${HOODIE}




